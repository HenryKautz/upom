RAE and UPOM together form a refinement acting-and-planning engine with multiple stacks (one for each incoming external task). Each stack is implemented as a Python thread.

We have the following domains in the domains folder.

1. domain_chargeableRobot: A chargeable robot collecting different objects
2. domain_springDoor: Robot needs to collect objects in an environment with spring doors
3. domain_exploreEnv: Robots and UAV move through an area and collects various data
4. domain_searchAndRescue: Robots perform search and rescue operations in the location of a natural disaster
5. domain_orderFulfillment: Robots are in a factory packing, wrapping and delivering packages

HOW TO USE?
To test on any domain, use the following command in terminal

cd upom/RAE_and_UPOM

python3 testRAEandRAEplan.py [-h] [--v V] [--domain D] [--problem P] [--plan S] [--c C] 
optional arguments:
  -h, --help  	show this help message and exit
  --v V      	verbosity of RAE and UPOM's debugging output (0, 1 or 2)
  --domain D    domain code of the test domain (CR, SD, EE, SR) (see below)
  --problem P   problem id for the problem eg. 'problem1', 'problem2', etc. The problem id should correspond to a
                problem inside the folder 'shared/problems/auto'.
  --plan pl   	Do you want to use planning or not? ('y' or 'n')
  --c C      	Mode of the clock ('Counter' or 'Clock')	

domain codes are as follows:
domain_chargeableRobot: 'CR',
domain_springDoor: 'SD',
domain_exploreEnv: 'EE',
domain_searchAndRescue: 'SR'
domain_orderFulfillment: 'OF'


HOW TO ADD NEW PROBLEMS? 
A problem file (Please go inside the folder problems/domain to view one) specifies the initial state, the tasks arriving at different times and various parameters specific to the domain. To define a new problem, please follow the
following syntax to name the file.

problemId_domainCode.py

For example, a problem of SD domain with problemId 'problem1001' should be named problem1001_SD.py.
To test problem1001 of Spring door, use the command:

python3 testRAEandRAEplan.py --domain SD --problem problem1001

The commands can be executed in two modes: 'Clock' or 'Counter'.
By default, the mode is set to 'Counter'.
Note, the clock mode has not been tested recently, and might lead to errors. 

Please contact me at patras@umd.edu if you encounter any problems.